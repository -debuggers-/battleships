﻿/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');
var ts = require("gulp-typescript");

gulp.task("default", function () {
    var tsResult = gulp.src(["scripts/typings/Hubs.d.ts", "assets/**/*.ts"])
        .pipe(ts({
            noImplicitAny: true,
            out: "output.js"
        }));

    return tsResult.js.pipe(gulp.dest('scripts'));
});