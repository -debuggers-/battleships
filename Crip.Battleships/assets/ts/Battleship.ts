﻿/// <reference path="../../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../../scripts/typings/signalr/signalr.d.ts" />

export class Battleship {

    hub: BattleshipsHub;
    static server: BattleshipsHubServer;
    client: any;

    constructor(hub: BattleshipsHub) {
        this.hub = hub;
        this.client = hub.client;
        Battleship.server = hub.server;

        this.registerMethods();
    }

    /**
     * Start battleship events when client connection established
     */
    public start() {
        $('#sendmessage').click(function () {
            Battleship.server.send("test user", $('#message').val());
        });
    }

    private registerMethods() {
        this.client.broadcastMessage = function (name: string, message: string) {

            var encodedName = $('<div />').text(name).html();
            var encodedMsg = $('<div />').text(message).html();
            // Add the message to the page. 
            $('#discussion').append('<li><strong>' + encodedName
                + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
        };
    }
}

