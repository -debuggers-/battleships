System.register(["Battleship"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Battleship_1;
    var App;
    return {
        setters:[
            function (Battleship_1_1) {
                Battleship_1 = Battleship_1_1;
            }],
        execute: function() {
            App = (function () {
                function App() {
                    App.hub = new Battleship_1.Battleship($.connection.battleshipsHub);
                    $.connection.hub.start().done(App.hub.start);
                }
                return App;
            }());
            exports_1("App", App);
        }
    }
});
