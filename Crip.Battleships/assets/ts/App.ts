﻿/// <reference path="../../scripts/typings/jquery/jquery.d.ts" />
import { Battleship } from "Battleship";

export class App {
    static hub: Battleship;

    constructor() {
        App.hub = new Battleship($.connection.battleshipsHub);

        $.connection.hub.start().done(App.hub.start);
    }
}