/// <reference path="../../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../../scripts/typings/signalr/signalr.d.ts" />
System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Battleship;
    return {
        setters:[],
        execute: function() {
            Battleship = (function () {
                function Battleship(hub) {
                    this.hub = hub;
                    this.client = hub.client;
                    Battleship.server = hub.server;
                    this.registerMethods();
                }
                /**
                 * Start battleship events when client connection established
                 */
                Battleship.prototype.start = function () {
                    $('#sendmessage').click(function () {
                        Battleship.server.send("test user", $('#message').val());
                    });
                };
                Battleship.prototype.registerMethods = function () {
                    this.client.broadcastMessage = function (name, message) {
                        var encodedName = $('<div />').text(name).html();
                        var encodedMsg = $('<div />').text(message).html();
                        // Add the message to the page. 
                        $('#discussion').append('<li><strong>' + encodedName
                            + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
                    };
                };
                return Battleship;
            }());
            exports_1("Battleship", Battleship);
        }
    }
});
