﻿// Get signalr.d.ts.ts from https://github.com/borisyankov/DefinitelyTyped (or delete the reference)
/// <reference path="signalr/signalr.d.ts" />
/// <reference path="jquery/jquery.d.ts" />

////////////////////
// available hubs //
////////////////////
//#region available hubs

interface SignalR {

    /**
      * The hub implemented by Crip.Battleships.Hubs.BattleshipsHub
      */
    battleshipsHub : BattleshipsHub;
}
//#endregion available hubs

///////////////////////
// Service Contracts //
///////////////////////
//#region service contracts

//#region BattleshipsHub hub

interface BattleshipsHub {
    
    /**
      * This property lets you send messages to the BattleshipsHub hub.
      */
    server : BattleshipsHubServer;

    /**
      * The functions on this property should be replaced if you want to receive messages from the BattleshipsHub hub.
      */
    client : any;
}

interface BattleshipsHubServer {

    /** 
      * Sends a "joinRoom" message to the BattleshipsHub hub.
      * Contract Documentation: ---
      * @param roomName {string} 
      * @return {JQueryPromise of void}
      */
    joinRoom(roomName : string) : JQueryPromise<void>

    /** 
      * Sends a "leaveRoom" message to the BattleshipsHub hub.
      * Contract Documentation: ---
      * @param roomName {string} 
      * @return {JQueryPromise of void}
      */
    leaveRoom(roomName : string) : JQueryPromise<void>

    /** 
      * Sends a "send" message to the BattleshipsHub hub.
      * Contract Documentation: ---
      * @param name {string} 
      * @param message {string} 
      * @return {JQueryPromise of void}
      */
    send(name : string, message : string) : JQueryPromise<void>
}

//#endregion BattleshipsHub hub

//#endregion service contracts



////////////////////
// Data Contracts //
////////////////////
//#region data contracts

//#endregion data contracts

